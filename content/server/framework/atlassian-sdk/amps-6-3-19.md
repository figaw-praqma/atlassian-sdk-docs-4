---
category: devguide
date: '2018-09-25'
platform: server
product: atlassian-sdk
subcategory: updates
title: AMPS 6.3.19
---
# AMPS 6.3.19

### Release Date: September 25, 2018


### Bug fixes:

- [AMPS-1327](https://ecosystem.atlassian.net/browse/AMPS-1327}) - atlas-run-standalone warns about deprecation of atlas-cli and fastdev
- [AMPS-1454](https://ecosystem.atlassian.net/browse/AMPS-1454}) - biz.aQute.bndlib:2.4.1-pr-916-atlassian-002 usage chokes on artifacts containing a module-info.class

### Improvements:

- [AMPS-1455](https://ecosystem.atlassian.net/browse/AMPS-1455}) - Bump tomcat 9 version to fix socket reset exception in maven-confluence-plugin build failure
- [AMPS-1456](https://ecosystem.atlassian.net/browse/AMPS-1456}) - Don't Ask Users To Join The Mailing List